﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HouseholdBudgeter.Helpers
{
    public static class UrlGuidBase64
    {
        public static string NewId()
        {
            var id = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
            return id.Replace("/", "_").Replace("+", "-").Replace("=", string.Empty);
        }
    }
}