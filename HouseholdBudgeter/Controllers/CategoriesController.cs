﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using HouseholdBudgeter.Models;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace HouseholdBudgeter.Controllers
{
    [RoutePrefix("api/Categories")]
    public class CategoriesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Catetories/Uid
        [HttpGet]
        [Route("{uid}", Name = "GetCategoryByUid")]
        [ResponseType(typeof(CategoryViewModel))]
        public async Task<IHttpActionResult> GetCategoryByUidAsync(string uid)
        {
            var userId = User.Identity.GetUserId();
            var vm = await db.Categories
                .Where(c => c.Uid == uid && 
                    c.Household.Members.Any(m => m.Id == userId))
                .ProjectTo<CategoryViewModel>()
                .FirstOrDefaultAsync();
            if (vm == null) return NotFound();
            return Ok(vm);
        }

        // POST: api/Catetories
        [HttpPost]
        [Route("")]
        [ResponseType(typeof(CategoryViewModel))]
        public async Task<IHttpActionResult> PostHouseholdAsync(CteateCategoryBindingModel model)
        {
            var userId = User.Identity.GetUserId();
            var household = await db.Households
                .Include(h => h.Categories)
                .FirstOrDefaultAsync(h => h.Uid == model.HouseholdUid && h.CreatorId == userId);
            if (household == null) return NotFound();

            var category = Mapper.Map<CteateCategoryBindingModel, Category>(model);
            if (household.Categories.Any(c => c.Name == category.Name))
            {
                return BadRequest("The Name is already taken.");
            }

            category.Household = household;
            db.Categories.Add(category);
            await db.SaveChangesAsync();
            var vm = Mapper.Map<Category, CategoryViewModel>(category);
            return CreatedAtRoute("GetCategoryByUid", new { uid = category.Uid }, vm);
        }

        // PUT: api/Catetories/Uid
        [HttpPut]
        [Route("{uid}")]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCatetoryAsync(string uid, [FromBody] UpdateCategoryBindingModel model)
        {
            var userId = User.Identity.GetUserId();
            var category = await db.Categories
                .Include(c => c.Household)
                .FirstOrDefaultAsync(c => c.Uid == uid &&
                    c.Household.CreatorId == userId);
            if (category == null) return NotFound();

            if (category.Name != model.Name)
            {
                if (category.Household.Categories.Any(c => c.Name == model.Name))
                {
                    return BadRequest("The Name is already taken.");
                }
                category.Name = model.Name;
                await db.SaveChangesAsync();
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: api/Catetories/Uid
        [HttpDelete]
        [Route("{uid}")]
        [ResponseType(typeof(HouseholdViewModel))]
        public async Task<IHttpActionResult> DeleteCategoryAsync(string uid)
        {
            var userId = User.Identity.GetUserId();
            var category = await db.Categories
                .FirstOrDefaultAsync(c => c.Uid == uid &&
                    c.Household.CreatorId == userId);
            if (category == null) return NotFound();
            if (category.Transactions.Any())
            {
                return BadRequest("Canot delete a category that has transactions.");
            }

            var vm = Mapper.Map<Category, CategoryViewModel>(category);
            db.Categories.Remove(category);
            await db.SaveChangesAsync();
            return Ok(vm);
        }

        // GET: api/Catetories/Uid/transactions 
        [HttpGet]
        [Route("{uid}/transactions")]
        [ResponseType(typeof(List<TransactionViewModel>))]
        public async Task<IHttpActionResult> GetCategoryTransactionsAsync(string uid)
        {
            // TODO: return 404 if the category doesn't exist
            var userId = User.Identity.GetUserId();
            var vm = await db.Transactions
                .Where(t => t.Category.Uid == uid &&
                    t.Category.Household.Members.Any(m => m.Id == userId))
                .Include(t => t.HAccount)
                .Include(t => t.Category)
                .ProjectTo<TransactionViewModel>()
                .ToListAsync();
            return Ok(vm);
        }
    }
}
