﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using HouseholdBudgeter.Models;
using Microsoft.AspNet.Identity;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace HouseholdBudgeter.Controllers
{
    [RoutePrefix("api/Invitations")]
    public class InvitationsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Invitations/Uid
        [HttpGet]
        [Route("{uid}", Name = "GetInvitationByUid")]
        [ResponseType(typeof(InvitationViewModel))]
        public async Task<IHttpActionResult> GetInvitationByUid(string uid)
        {
            var userId = User.Identity.GetUserId();
            var vm = await db.Invitations
                .Where(i => i.Uid == uid &&
                i.Household.CreatorId == userId)
                .ProjectTo<InvitationViewModel>()
                .FirstOrDefaultAsync();
            if (vm == null) return NotFound();
            return Ok(vm);
        }

        // POST: api/Invitations
        [HttpPost]
        [Route("")]
        [ResponseType(typeof(InvitationViewModel))]
        public async Task<IHttpActionResult> PostInvitationAsync([FromBody] InvitationBindingModel model)
        {
            var userId = User.Identity.GetUserId();
            var household = await db.Households
                .Include(h => h.Members)
                .Include(h => h.Invitations)
                .FirstOrDefaultAsync(h => h.Uid == model.HouseholdUid && h.CreatorId == userId);
            if (household == null) return NotFound();

            var invitee = await db.Users
                .FirstOrDefaultAsync(u => u.Email == model.InviteeEmail);
            if (invitee == null) return NotFound();
            if (household.Members.Any(m => m.Id == invitee.Id))
            {
                return BadRequest("The user is already a member of your household.");
            }
            if (household.Invitations.Any(i => i.InviteeId == invitee.Id))
            {
                return BadRequest("The user has already been invited.");
            }

            var invitation = new Invitation
            {
                Household = household,
                Invitee = invitee
            };
            db.Invitations.Add(invitation);
            await db.SaveChangesAsync();

            var vm = Mapper.Map<Invitation, InvitationViewModel>(invitation);
            return CreatedAtRoute("GetInvitationByUid", new { uid = invitation.Uid }, vm);
        }

        // DELETE: api/Invitations/Uid
        [HttpDelete]
        [Route("{uid}")]
        [ResponseType(typeof(InvitationViewModel))]
        public async Task<IHttpActionResult> DeleteInvitationAsync(string uid)
        {
            var userId = User.Identity.GetUserId();
            var invitation = await db.Invitations
                .Include(i => i.Household)
                .FirstOrDefaultAsync(i => i.Household.CreatorId == userId);
            if (invitation == null) return NotFound();

            var vm = Mapper.Map<Invitation, InvitationViewModel>(invitation);
            db.Invitations.Remove(invitation);
            await db.SaveChangesAsync();
            return Ok(vm);
        }

        // POST: api/invitations/Uid/acceptance 
        [HttpPost]
        [Route("{uid}/acceptance")]
        [ResponseType(typeof(HouseholdViewModel))]
        public async Task<IHttpActionResult> AcceptInvitationAsync(string uid)
        {
            var userId = User.Identity.GetUserId();
            var invitation = await db.Invitations
                .Include(i => i.Household)
                .Include(i => i.Invitee)
                .FirstOrDefaultAsync(i =>
                    i.Uid == uid &&
                    !i.Household.Members.Any(m => m.Id == userId) &&
                    i.InviteeId == userId);
            if (invitation == null) return NotFound();

            var household = invitation.Household;
            household.Members.Add(invitation.Invitee);
            db.Invitations.Remove(invitation);
            await db.SaveChangesAsync();
            var vm = Mapper.Map<Household, HouseholdViewModel>(household);
            return CreatedAtRoute("GetHouseholdByUid", new { uid = vm.Uid }, vm);
        }
    }
}
