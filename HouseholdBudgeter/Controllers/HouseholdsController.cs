﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using HouseholdBudgeter.Models;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace HouseholdBudgeter.Controllers
{
    [RoutePrefix("api/Households")]
    public class HouseholdsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Households/Uid
        [HttpGet]
        [Route("{uid}", Name = "GetHouseholdByUid")]
        [ResponseType(typeof(HouseholdViewModel))]
        public async Task<IHttpActionResult> GetHouseholdByUidAsync(string uid)
        {
            var userId = User.Identity.GetUserId();
            var vm = await db.Households
                .Where(h => h.Uid == uid && h.Members.Any(m => m.Id == userId))
                .ProjectTo<HouseholdViewModel>()
                .FirstOrDefaultAsync();
            if (vm == null) return NotFound();
            return Ok(vm);
        }

        // POST: api/Households
        [HttpPost]
        [Route("")]
        [ResponseType(typeof(HouseholdViewModel))]
        public async Task<IHttpActionResult> PostHouseholdAsync(HouseholdBindingModel model)
        {
            var household = Mapper.Map<HouseholdBindingModel, Household>(model);
            if (db.Households.Any(h => h.Name == household.Name))
            {
                return BadRequest("The Name is already taken.");
            }
            var userId = User.Identity.GetUserId();
            var user = await db.Users.FirstOrDefaultAsync(u => u.Id == userId);
            household.Creator = user;
            household.Members.Add(user);
            db.Households.Add(household);
            await db.SaveChangesAsync();
            var vm = Mapper.Map<Household, HouseholdViewModel>(household);
            return CreatedAtRoute("GetHouseholdByUid", new { uid = household.Uid }, vm);
        }

        // PUT: api/Households/uid
        [HttpPut]
        [Route("{uid}")]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutHouseholdAsync(string uid, [FromBody] HouseholdBindingModel model)
        {
            var userId = User.Identity.GetUserId();
            var household = await db.Households
                .FirstOrDefaultAsync(h => h.Uid == uid && h.CreatorId == userId);
            if (household == null) return NotFound();

            if (household.Name != model.Name)
            {
                if (db.Households.Any(h => h.Name == model.Name))
                {
                    return BadRequest("The Name is already taken.");
                }
                household.Name = model.Name;
                await db.SaveChangesAsync();
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: api/Households/uid
        [HttpDelete]
        [Route("{uid}")]
        [ResponseType(typeof(HouseholdViewModel))]
        public async Task<IHttpActionResult> DeleteHouseholdAsync(string uid)
        {
            var userId = User.Identity.GetUserId();
            var household = await db.Households
                .FirstOrDefaultAsync(h => h.Uid == uid && h.CreatorId == userId);
            if (household == null) return NotFound();
            
            var vm = Mapper.Map<Household, HouseholdViewModel>(household);
            db.Households.Remove(household);
            await db.SaveChangesAsync();

            return Ok(vm);
        }

        // DELETE: api/Households/uid/Leave
        [HttpDelete]
        [Route("{uid}/Leave")]
        public async Task<IHttpActionResult> LeaveHouseholdAsync(string uid)
        {
            var userId = User.Identity.GetUserId();
            var data = await db.Households
                .Where(h => h.Uid == uid)
                .Select(h => new
                {
                    Household = h,
                    User = h.Members.FirstOrDefault(m => m.Id == userId)
                })
                .FirstOrDefaultAsync();
            if (data == null || data.Household == null || data.User == null)
            {
                return NotFound();
            }
            if (data.Household.CreatorId == userId)
            {
                return BadRequest("The creator cannot leave their household.");
            }
            data.Household.Members.Remove(data.User);
            await db.SaveChangesAsync();
            return Ok();
        }

        // GET: api/Households/uid/Members
        [HttpGet]
        [Route("{uid}/Members")]
        [ResponseType(typeof(List<UserViewModel>))]
        public async Task<IHttpActionResult> GetHouseholdMembersAsync(string uid)
        {
            var userId = User.Identity.GetUserId();
            var household = await db.Households
                .Include(h => h.Members)
                .FirstOrDefaultAsync(h =>
                    h.Uid == uid &&
                    h.Members.Any(m => m.Id == userId));
            if (household == null) return NotFound();
            var vm = Mapper.Map<ICollection<ApplicationUser>, List<UserViewModel>>(household.Members);
            return Ok(vm);
        }

        // DELETE: api/Households/uid/removeMember
        [HttpDelete]
        [Route("{uid}/removeMember")]
        [ResponseType(typeof(UserViewModel))]
        public async Task<IHttpActionResult> DeleteHouseholdMemberAsync(string uid, [FromBody] RemoveMemberBindingModel member)
        {
            var userId = User.Identity.GetUserId();
            if (member.MemberId == userId)
            {
                return BadRequest("Cannot remove yourself.");
            }

            var data = await db.Households
                .Where(h => h.Uid == uid && h.CreatorId == userId)
                .Select(h => new
                {
                    Household = h,
                    Member = h.Members.FirstOrDefault(m => m.Id == member.MemberId)
                })
                .FirstOrDefaultAsync();
            if (data == null || data.Household == null || data.Member == null)
            {
                return NotFound();
            }
            
            var vm = Mapper.Map<ApplicationUser, UserViewModel>(data.Member);
            data.Household.Members.Remove(data.Member);
            await db.SaveChangesAsync();
            return Ok(vm);
        }

        // GET: api/Households/uid/Invitations
        [HttpGet]
        [Route("{uid}/Invitations")]
        [ResponseType(typeof(List<InvitationViewModel>))]
        public async Task<IHttpActionResult> GetHouseholdInvitationsAsync(string uid)
        {
            var userId = User.Identity.GetUserId();
            var household = await db.Households
                .Include(h => h.Invitations)
                .FirstOrDefaultAsync(h => h.Uid == uid && h.CreatorId == userId);

            if (household == null) return NotFound();
            var vm = Mapper.Map<ICollection<Invitation>, List<InvitationViewModel>>(household.Invitations);
            return Ok(vm);
        }

        // GET: api/Households/uid/Categories
        [HttpGet]
        [Route("{uid}/Categories")]
        [ResponseType(typeof(List<CategoryViewModel>))]
        public async Task<IHttpActionResult> GetHouseholdCategoriesAsync(string uid)
        {
            var userId = User.Identity.GetUserId();
            var household = await db.Households
                .Include(h => h.Categories)
                .FirstOrDefaultAsync(h => h.Uid == uid &&
                    h.Members.Any(m => m.Id == userId));

            if (household == null) return NotFound();
            var vm = Mapper.Map<ICollection<Category>, List<CategoryViewModel>>(household.Categories);
            return Ok(vm);
        }

        // GET: api/Households/uid/HAccounts
        [HttpGet]
        [Route("{uid}/HAccounts")]
        [ResponseType(typeof(List<HAccountViewModel>))]
        public async Task<IHttpActionResult> GetHouseholdHAccountsAsync(string uid)
        {
            var userId = User.Identity.GetUserId();
            var household = await db.Households
                .Include(h => h.HAccounts)
                .FirstOrDefaultAsync(h => h.Uid == uid &&
                    h.Members.Any(m => m.Id == userId));

            if (household == null) return NotFound();
            var vm = Mapper.Map<ICollection<HAccount>, List<HAccountViewModel>>(household.HAccounts);
            return Ok(vm);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}