﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using HouseholdBudgeter.Models;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace HouseholdBudgeter.Controllers
{
    [RoutePrefix("api/HAccounts")]
    public class HAccountsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/HAccounts/Uid
        [HttpGet]
        [Route("{uid}", Name = "GetHAccountByUid")]
        [ResponseType(typeof(HAccountViewModel))]
        public async Task<IHttpActionResult> GetHAccountByUidAsync(string uid)
        {
            var userId = User.Identity.GetUserId();
            var vm = await db.HAccounts
                .Where(a => a.Uid == uid &&
                    a.Household.Members.Any(m => m.Id == userId))
                .ProjectTo<HAccountViewModel>()
                .FirstOrDefaultAsync();
            if (vm == null) return NotFound();
            return Ok(vm);
        }

        // POST: api/HAccounts
        [HttpPost]
        [Route("")]
        [ResponseType(typeof(HAccountViewModel))]
        public async Task<IHttpActionResult> PostHAccountAsync(CreateHAccountBindingModel model)
        {
            var userId = User.Identity.GetUserId();
            var household = await db.Households
                .Include(h => h.HAccounts)
                .FirstOrDefaultAsync(h => h.Uid == model.HouseholdUid && h.CreatorId == userId);
            if (household == null) return NotFound();

            var account = Mapper.Map<CreateHAccountBindingModel, HAccount>(model);
            if (household.HAccounts.Any(a => a.Name == model.Name))
            {
                return BadRequest("The Name is already taken.");
            }

            account.Household = household;
            db.HAccounts.Add(account);
            await db.SaveChangesAsync();
            var vm = Mapper.Map<HAccount, HAccountViewModel>(account);
            return CreatedAtRoute("GetHAccountByUid", new { uid = account.Uid }, vm);
        }

        // PUT: api/HAccounts
        [HttpPut]
        [Route("{uid}")]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutHAccountAsync(string uid, [FromBody] UpdateHAccountBindingModel model)
        {
            var userId = User.Identity.GetUserId();
            var account = await db.HAccounts
                .Include(a => a.Household)
                .FirstOrDefaultAsync(a => a.Uid == uid &&
                    a.Household.CreatorId == userId);
            if (account == null) return NotFound();

            if (account.Name != model.Name)
            {
                if (account.Household.HAccounts.Any(c => c.Name == model.Name))
                {
                    return BadRequest("The Name is already taken.");
                }
                account.Name = model.Name;
                await db.SaveChangesAsync();
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: api/HAccounts/Uid
        [HttpDelete]
        [Route("{uid}")]
        [ResponseType(typeof(HAccountViewModel))]
        public async Task<IHttpActionResult> DeleteHAccountAsync(string uid)
        {
            var userId = User.Identity.GetUserId();
            var account = await db.HAccounts
                .FirstOrDefaultAsync(a => a.Uid == uid &&
                    a.Household.CreatorId == userId);
            if (account == null) return NotFound();

            var vm = Mapper.Map<HAccount, HAccountViewModel>(account);
            db.HAccounts.Remove(account);
            await db.SaveChangesAsync();
            return Ok(vm);
        }

        // GET: api/HAccounts/Uid/transactions 
        [HttpGet]
        [Route("{uid}/transactions")]
        [ResponseType(typeof(List<TransactionViewModel>))]
        public async Task<IHttpActionResult> GetHAccountTransactionsAsync(string uid)
        {
            // TODO: return 404 if the account doesn't exist
            var userId = User.Identity.GetUserId();
            var vm = await db.Transactions
                .Where(t => t.HAccount.Uid == uid &&
                    t.HAccount.Household.Members.Any(m => m.Id == userId))
                .Include(t => t.HAccount)
                .Include(t => t.Category)
                .ProjectTo<TransactionViewModel>()
                .ToListAsync();
            return Ok(vm);
        }

        // POST: api/HAccounts/Uid/recalculation 
        [HttpPost]
        [Route("{uid}/recalculation")]
        [ResponseType(typeof(HAccountViewModel))]
        public async Task<IHttpActionResult> RecalculateAsync(string uid)
        {
            var userId = User.Identity.GetUserId();
            var account = await db.HAccounts
                .Where(a => a.Uid == uid &&
                    a.Household.Members.Any(m => m.Id == userId))
                .Include(a => a.Transactions)
                .Include(a => a.Household)
                .FirstOrDefaultAsync();
            if (account == null) return NotFound();

            account.Balance = account.Transactions
                .Where(t => !t.IsVoid).Sum(t => t.Amount);
            await db.SaveChangesAsync();

            var vm = Mapper.Map<HAccount, HAccountViewModel>(account);
            return Ok(vm);
        }
    }
}
