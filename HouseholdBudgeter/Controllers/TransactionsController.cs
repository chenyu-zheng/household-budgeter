﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using HouseholdBudgeter.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace HouseholdBudgeter.Controllers
{
    [RoutePrefix("api/Transactions")]
    public class TransactionsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Transactions/Uid
        [HttpGet]
        [Route("{uid}", Name = "GetTransactionByUid")]
        [ResponseType(typeof(TransactionViewModel))]
        public async Task<IHttpActionResult> GetTransactionByUid(string uid)
        {
            var userId = User.Identity.GetUserId();
            var vm = await db.Transactions
                .Where(t => t.Uid == uid &&
                    t.HAccount.Household.Members.Any(m => m.Id == userId))
                .ProjectTo<TransactionViewModel>()
                .FirstOrDefaultAsync();
            if (vm == null) return NotFound();
            return Ok(vm);
        }

        // POST: api/Transactions
        [HttpPost]
        [Route("")]
        [ResponseType(typeof(TransactionViewModel))]
        public async Task<IHttpActionResult> PostTransactionAsync(CreateTransactionBindingModel model)
        {
            // Negative amount is allowed for withdrawals.
            if (Math.Abs(model.Amount.Value) < 0.01m)
            {
                return BadRequest("Transaction amount cannot be lower than 0.01.");
            }
            var userId = User.Identity.GetUserId();
            var data = await db.Households
                .Where(h => h.Uid == model.HouseholdUid && 
                    h.Members.Any(m => m.Id == userId))
                .Select(h => new
                {
                    Household = h,
                    HAccount = h.HAccounts
                        .FirstOrDefault(a => a.Uid == model.HAccountUid && 
                            a.Household.Uid == model.HouseholdUid),
                    Category = h.Categories.
                        FirstOrDefault(c => c.Uid == model.CategoryUid && 
                            c.Household.Uid == model.HouseholdUid)
                })
                .FirstOrDefaultAsync();

            if (data == null) return NotFound();
            if (data.HAccount == null)
            {
                return BadRequest("The household account does not exist.");
            }
            if (data.Category == null)
            {
                return BadRequest("The category does not exist.");
            }

            var transaction = Mapper.Map<CreateTransactionBindingModel, Transaction>(model);
            transaction.Category = data.Category;
            transaction.HAccount = data.HAccount;
            transaction.CreatorId = userId;
            db.Transactions.Add(transaction);
            data.HAccount.Balance = data.HAccount.Transactions
                .Where(t => !t.IsVoid).Sum(t => t.Amount);
            await db.SaveChangesAsync();

            var vm = Mapper.Map<Transaction, TransactionViewModel>(transaction);
            return CreatedAtRoute("GetTransactionByUid", new { uid = vm.Uid }, vm);
        }

        // PUT: api/Transactions/Uid
        [HttpPut]
        [Route("{uid}")]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutTransactionAsync(string uid, [FromBody] UpdateTransactionBindingModel model)
        {
            var userId = User.Identity.GetUserId();
            var transaction = await db.Transactions
                .Include(a => a.Category)
                .FirstOrDefaultAsync(t => t.Uid == uid &&
                    t.Category.Household.Members.Any(m => m.Id == userId));
            if (transaction == null) return NotFound();

            if (transaction.Category.Uid != model.CategoryUid)
            {
                var newCategory = await db.Categories.FirstOrDefaultAsync(c => c.Uid == model.CategoryUid);
                transaction.Category = newCategory;
            }
            transaction.Description = model.Description;
            await db.SaveChangesAsync();
            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Transactions/Uid/Void 
        [HttpPost]
        [Route("{uid}/Void")]
        [ResponseType(typeof(HAccountViewModel))]
        public async Task<IHttpActionResult> VoidTransactionAsync(string uid)
        {
            var userId = User.Identity.GetUserId();
            var transaction = await db.Transactions
                .Include(a => a.HAccount)
                .FirstOrDefaultAsync(t => t.Uid == uid &&
                    (t.CreatorId == userId ||
                        t.HAccount.Household.CreatorId == userId));

            if (transaction == null) return NotFound();
            if (transaction.IsVoid)
            {
                return BadRequest("The transaction is already void.");
            }
            transaction.IsVoid = true;
            transaction.HAccount.Balance = transaction.HAccount.Transactions
                .Where(t => !t.IsVoid).Sum(t => t.Amount);
            await db.SaveChangesAsync();
            var vm = Mapper.Map<Transaction, TransactionViewModel>(transaction);
            return Ok(vm);
        }
    }
}
