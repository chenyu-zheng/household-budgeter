﻿using AutoMapper.QueryableExtensions;
using HouseholdBudgeter.Models;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace HouseholdBudgeter.Controllers
{
    [RoutePrefix("api/me")]
    public class CurrentUserController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        //GET: api/me/invitations
        [HttpGet]
        [Route("invitations")]
        [ResponseType(typeof(List<InvitationViewModel>))]
        public async Task<IHttpActionResult> GetInvitationsAsync()
        {
            var userId = User.Identity.GetUserId();
            var vm = await db.Invitations
                .Where(i => i.InviteeId == userId)
                .ProjectTo<InvitationViewModel>()
                .ToListAsync();

            return Ok(vm);
        }

        //GET: api/me/households/created
        [HttpGet]
        [Route("households/created")]
        [ResponseType(typeof(List<HouseholdViewModel>))]
        public async Task<IHttpActionResult> GetCreatedHouseholdsAsync()
        {
            var userId = User.Identity.GetUserId();
            var vm = await db.Households
                .Where(h => h.CreatorId == userId)
                .ProjectTo<HouseholdViewModel>()
                .ToListAsync();

            return Ok(vm);
        }

        //GET: api/me/households/joined
        [HttpGet]
        [Route("households/joined")]
        [ResponseType(typeof(List<HouseholdViewModel>))]
        public async Task<IHttpActionResult> GetJoinedHouseholdsAsync()
        {
            var userId = User.Identity.GetUserId();
            var vm = await db.Households
                .Where(h => h.Members.Any(m => m.Id == userId))
                .ProjectTo<HouseholdViewModel>()
                .ToListAsync();

            return Ok(vm);
        }
    }
}
