﻿using AutoMapper;
using HouseholdBudgeter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HouseholdBudgeter
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Household, HouseholdViewModel>();
            CreateMap<ApplicationUser, UserViewModel>()
                .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.FirstName + " " + src.LastName));
            CreateMap<HouseholdBindingModel, Household>();
            CreateMap<Invitation, InvitationViewModel>();
            CreateMap<Category, CategoryViewModel>();
            CreateMap<CteateCategoryBindingModel, Category>();
            CreateMap<HAccount, HAccountViewModel>();
            CreateMap<CreateHAccountBindingModel, HAccount>();
            CreateMap<Transaction, TransactionViewModel>()
                .ForMember(dest => dest.CreatorFullName, opt => opt.MapFrom(src => src.Creator.FirstName + " " + src.Creator.LastName));
            CreateMap<CreateTransactionBindingModel, Transaction>();
        }
    }
}