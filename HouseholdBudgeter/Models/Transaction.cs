﻿using HouseholdBudgeter.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HouseholdBudgeter.Models
{
    public class Transaction
    {
        public int Id { get; set; }
        public string Uid { get; set; }
        public DateTimeOffset Created { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }
        public bool IsVoid { get; set; }
        public int HAccountId { get; set; }
        public virtual HAccount HAccount { get; set; }
        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }
        public string CreatorId { get; set; }
        public virtual ApplicationUser Creator { get; set; }

        public Transaction()
        {
            Uid = UrlGuidBase64.NewId();
            Created = DateTimeOffset.Now;
            IsVoid = false;
        }
    }
}