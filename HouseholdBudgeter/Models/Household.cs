﻿using HouseholdBudgeter.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HouseholdBudgeter.Models
{
    public class Household
    {
        public int Id { get; set; }
        public string Uid { get; set; }
        public string Name { get; set; }
        public DateTimeOffset Created { get; set; }
        public string CreatorId { get; set; }
        public virtual ApplicationUser Creator { get; set; }
        public virtual ICollection<ApplicationUser> Members { get; set; }
        public virtual ICollection<Invitation> Invitations { get; set; }
        public virtual ICollection<Category> Categories { get; set; }
        public virtual ICollection<HAccount> HAccounts { get; set; }

        public Household()
        {
            Uid = UrlGuidBase64.NewId();
            Created = DateTimeOffset.Now;
            Members = new HashSet<ApplicationUser>();
            Invitations = new HashSet<Invitation>();
            Categories = new HashSet<Category>();
            HAccounts = new HashSet<HAccount>();
        }
    }
}