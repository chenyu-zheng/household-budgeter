﻿using HouseholdBudgeter.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HouseholdBudgeter.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string Uid { get; set; }
        public string Name { get; set; }
        public int HouseholdId { get; set; }
        public virtual Household Household { get; set; }
        public virtual ICollection<Transaction> Transactions { get; set; }

        public Category()
        {
            Uid = UrlGuidBase64.NewId();
            Transactions = new HashSet<Transaction>();
        }
    }
}