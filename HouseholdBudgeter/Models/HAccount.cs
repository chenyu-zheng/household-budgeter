﻿using HouseholdBudgeter.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HouseholdBudgeter.Models
{
    public class HAccount
    {
        public int Id { get; set; }
        public string Uid { get; set; }
        public string Name { get; set; }
        public decimal Balance { get; set; }
        public int HouseholdId { get; set; }
        public virtual Household Household { get; set; }
        public virtual ICollection<Transaction> Transactions { get; set; }

        public HAccount()
        {
            Uid = UrlGuidBase64.NewId();
            Transactions = new HashSet<Transaction>();
        }
    }
}