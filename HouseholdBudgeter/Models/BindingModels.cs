﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HouseholdBudgeter.Models
{
    public class HouseholdBindingModel
    {
        [Required]
        [StringLength(20, MinimumLength = 4, ErrorMessage = "The {0} must be between {2} and {1} characters long.")]
        [RegularExpression("^[a-zA-Z0-9]{1,}(?:[ -.][a-zA-Z0-9]+){0,}$", ErrorMessage = "Invalid {0}.")]
        public string Name { get; set; }
    }

    public class InvitationBindingModel
    {
        [Required]
        public string HouseholdUid { get; set; }

        [Required]
        [EmailAddress]
        public string InviteeEmail { get; set; }
    }

    public class RemoveMemberBindingModel
    {
        [Required]
        public string MemberId { get; set; }
    }

    public class CteateCategoryBindingModel
    {
        [Required]
        public string HouseholdUid { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 4, ErrorMessage = "The {0} must be between {2} and {1} characters long.")]
        [RegularExpression("^[a-zA-Z0-9]{1,}(?:[ -.][a-zA-Z0-9]+){0,}$", ErrorMessage = "Invalid {0}.")]
        public string Name { get; set; }
    }

    public class UpdateCategoryBindingModel
    {
        [Required]
        [StringLength(20, MinimumLength = 4, ErrorMessage = "The {0} must be between {2} and {1} characters long.")]
        [RegularExpression("^[a-zA-Z0-9]{1,}(?:[ -.][a-zA-Z0-9]+){0,}$", ErrorMessage = "Invalid {0}.")]
        public string Name { get; set; }
    }

    public class CreateHAccountBindingModel
    {
        [Required]
        public string HouseholdUid { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 4, ErrorMessage = "The {0} must be between {2} and {1} characters long.")]
        [RegularExpression("^[a-zA-Z0-9]{1,}(?:[ -.][a-zA-Z0-9]+){0,}$", ErrorMessage = "Invalid {0}.")]
        public string Name { get; set; }
    }

    public class UpdateHAccountBindingModel
    {
        [Required]
        [StringLength(20, MinimumLength = 4, ErrorMessage = "The {0} must be between {2} and {1} characters long.")]
        [RegularExpression("^[a-zA-Z0-9]{1,}(?:[ -.][a-zA-Z0-9]+){0,}$", ErrorMessage = "Invalid {0}.")]
        public string Name { get; set; }
    }

    public class CreateTransactionBindingModel
    {
        [Required]
        public decimal? Amount { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 1, ErrorMessage = "The {0} must be between {2} and {1} characters long.")]
        public string Description { get; set; }

        [Required]
        public string HouseholdUid { get; set; }

        [Required]
        public string HAccountUid { get; set; }

        [Required]
        public string CategoryUid { get; set; }
    }

    public class UpdateTransactionBindingModel
    {
        [Required]
        [StringLength(100, MinimumLength = 1, ErrorMessage = "The {0} must be between {2} and {1} characters long.")]
        public string Description { get; set; }

        [Required]
        public string CategoryUid { get; set; }
    }
}