﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HouseholdBudgeter.Models
{
    public class HouseholdViewModel
    {
        public string Uid { get; set; }
        public string Name { get; set; }
        public UserViewModel Creator { get; set; }
        public int MembersCount { get; set; }
    }

    public class UserViewModel
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
    }

    public class InvitationViewModel
    {
        public string Uid { get; set; }
        public string HouseholdUid { get; set; }
        public string HouseholdName { get; set; }
        public string InviteeEmail { get; set; }
    }

    public class HAccountViewModel
    {
        public string Uid { get; set; }
        public string Name { get; set; }
        public decimal Balance { get; set; }
        public int TransactionsCount { get; set; }
        public string HouseholdUid { get; set; }
    }

    public class CategoryViewModel
    {
        public string Uid { get; set; }
        public string Name { get; set; }
        public int TransactionsCount { get; set; }
        public string HouseholdUid { get; set; }
    }

    public class TransactionViewModel
    {
        public string Uid { get; set; }
        public DateTimeOffset Created { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }
        public bool IsVoid { get; set; }
        public string HAccountUid { get; set; }
        public string HAccountName { get; set; }
        public string CategoryUid { get; set; }
        public string CategoryName { get; set; }
        public string CreatorId { get; set; }
        public string CreatorFullName { get; set; }
    }
}