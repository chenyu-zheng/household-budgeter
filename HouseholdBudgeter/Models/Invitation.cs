﻿using HouseholdBudgeter.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HouseholdBudgeter.Models
{
    public class Invitation
    {
        public int Id { get; set; }
        public string Uid { get; set; }
        public DateTimeOffset Created { get; set; }
        public int HouseholdId { get; set; }
        public virtual Household Household { get; set; }
        public string InviteeId { get; set; }
        public virtual ApplicationUser Invitee { get; set; }

        public Invitation()
        {
            Uid = UrlGuidBase64.NewId();
            Created = DateTimeOffset.Now;
        }
    }
}